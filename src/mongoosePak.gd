extends Object
class_name MongoosePak

const MAGIC = 0x4D4B4D4E
const DEFAULT_TEMP_PATH = "user://paktemp/"

var package := {
	"Error": ERR_BUG,
	"PakPath": DEFAULT_TEMP_PATH,
	"Header": null
}

static func unpack(file: String, dest: String = DEFAULT_TEMP_PATH) -> MongoosePak:
	var result = MongoosePak.new()
	var err = OK
	result.package["Error"] = err
	return result

static func pack(file: String, dir_to_pack: String = DEFAULT_TEMP_PATH) -> int:
	var err = OK
	var writer = FileAccess.open(file, FileAccess.WRITE)
	if writer:
		writer.store_64(MAGIC)
		writer.store_64(0)
		var dir = DirAccess.open(dir_to_pack)
		if dir:
			
			var zipWriter = ZIPPacker.new()
			err = zipWriter.open("user://package.zip")
		else:
			err = DirAccess.get_open_error()
	else:
		err = FileAccess.get_open_error()
	return err

func get_error() -> int:
	return package["Error"]
